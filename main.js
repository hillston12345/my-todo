const Vue = require('vue')
const main = require('vue!./components/main.vue')

new Vue({
    el: '#app',
    components: {
        main
    }
})
